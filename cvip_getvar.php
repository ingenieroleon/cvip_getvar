<?php
/*
 * Plugin Name: cvip_getvar
 * Plugin URI: ColombiaVIP.com
 * Description: Sistema de gestion de loteria
 * Version: 1.20181011
 * Author: ColombiaVIP.com
 * Author URI: ColombiaVIP.com
 * License: PRIVADA
 */



defined ('ABSPATH') or die ("Ey!, que estas haciendo aqui? humano tonto >:(");

class cvip_getvar {
  function __construct() {

      # =============================================
      # =   SHORCODE PARA TRAER UNA VARIABLE DEL QUERYSTRING =
      # =============================================

      function shortcode_cvip_getvar($atts) {
        $atts = shortcode_atts( array(
          'name' => '',
		  'default' => 'Default',
        ), $atts);
		$name=$atts["name"];
		$default=$atts["default"];
		if (empty($name) OR empty ($_GET[$name])){
			return $default;
		}
		return $_GET[$name];
      }

      add_shortcode( 'cvip_getvar' , "shortcode_cvip_getvar" );

      


      # ==============================================
      # =              HOOK-PLUGIN                   =
      # = ACTIVACION, DESACTIVACION Y DESINSTALACION =
      # ==============================================

      # = ACTIVACION DEL PLUGIN  =
      register_activation_hook(__FILE__, array($this, 'activate'));

      # = DESACTIVACION DEL PLUGIN =
      register_deactivation_hook(__FILE__, array($this, 'deactivate'));

      # = DESINSTALACION DEL PLUGIN =
      register_uninstall_hook(__FILE__, array($this, 'uninstall'));
  }

  # ==============================================
  # =             FUNCIONES-PLUGIN               =
  # =      ACTIVAR, DESACTIVAR Y DESINTALAR      =
  # ==============================================

  function activate(){
    flush_rewrite_rules();
  }

  function deactivate(){
    flush_rewrite_rules();
  }

  function uninstall(){
	flush_rewrite_rules();

  }

}

if (class_exists('cvip_getvar')){
  $cvip_getvar = new cvip_getvar();
}